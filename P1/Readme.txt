﻿Họ tên: Đặng Sơn Tùng
MSSV: 1653101
Lớp: 16CLC2
Email: 1653101@student.hcmus.edu.vn

Project 2:
* Đã hoàn thành:
- Color và Font:
  + Color và Font được lưu trữ riêng cho mỗi child window, nghĩa là khi user vẽ trên child window nào thì sẽ dùng color và font của child window đó.
  + User chọn màu trên Color dialog.
  + User chọn font, size, style (bold, italic, underline) trên Font dialog

- Draw:
  + Các object đã vẽ (line, rectangle, ellipse, text) được lưu trữ riêng cho mỗi child window.
  + Line: vẽ đoạn thẳng trên child window. Màu vẽ là màu đã chọn ở chức năng Color.
  + Rectangle, Ellipse: tương tự như Line, nhưng vẽ hình chữ nhật hay ellipse.
  + Text: tạo một chuỗi text trên child window. Màu và font của text là màu và font đã chọn ở chức năng Color, Font. Text được hiển thị tại vị trí click mouse.

- Paint lại child window: vẽ lại các object có trong child window.

- Save:
  + Mở dialog Save as để user nhập tên file và lưu data của child window vào file. Gán tên file mới lên title của child window.
  + Nếu child window đã save rồi thì lưu data của child window vào file.

- Open:
  + Mở dialog Open để user chọn file *.drw.
  + Đọc file và hiển thị các data lên child window.
  + Gán tên file lên title của child window.

- Edit:
  + Click được vào hình để chọn và có thể di chuyển vị trí của hình (đường thẳng, chữ nhật và ellipse).

* Chưa hoàn thành:
- Edit:
  + Chưa click được vào text và để chọn và di chuyển.
  + Chưa cài đặt chức năng resize hình.