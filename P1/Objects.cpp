#include "stdafx.h"
#include "Objects.h"

void LINE::draw(HDC hdc)
{
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
}

void RECTANGLE::draw(HDC hdc)
{
	if (color != RGB(0, 0, 0)) {
		HBRUSH hbrush = CreateSolidBrush(color);
		SelectObject(hdc, hbrush); 
		Rectangle(hdc, left, top, right, bottom);
		hbrush = CreateSolidBrush(RGB(0, 0, 0));
		SelectObject(hdc, hbrush);
		DeleteObject(hbrush);
	}
	else {
		Rectangle(hdc, left, top, right, bottom);
	}
}

void ELLIPSE::draw(HDC hdc)
{
	if (color != RGB(0, 0, 0)) {
		HBRUSH hbrush = CreateSolidBrush(color);
		SelectObject(hdc, hbrush);
		Ellipse(hdc, left, top, right, bottom);
		hbrush = CreateSolidBrush(RGB(0, 0, 0));
		SelectObject(hdc, hbrush);
		DeleteObject(hbrush);
	}
	else {
		Ellipse(hdc, left, top, right, bottom);
	}
}

void TEXT::draw(HDC hdc)
{
	HFONT hFont = CreateFontIndirect(&font);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	TextOutW(hdc, left, top, (LPCWSTR)str, sizeOfText);
	DeleteObject(hFont);
}

bool LINE::checkPoint(int left, int top)
{
	float A = (float)(left - this->left) / (this->right - this->left);
	float B = (float)(top - this->top) / (this->bottom - this->top);
	if (A == B)
		return true;
	return false;
}

bool RECTANGLE::checkPoint(int left, int top)
{
	if (left > this->left && left< this->right && top>this->top && top < this->bottom)
		return true;
	return false;
}

bool ELLIPSE::checkPoint(int left, int top)
{
	int max, min, toadoX, toadoY;
	if (abs(this->right - this->left) < abs(this->bottom - this->top)) {
		max = abs(this->bottom - this->top);
		min = abs(this->right - this->left);
	}
	else {
		min = abs(this->bottom - this->top);
		max = abs(this->right - this->left);
	}

	float A = pow(left - ((this->right + this->left) / 2), 2) / (float)pow(max / 2, 2);
	float B = pow(top - ((this->bottom + this->top) / 2), 2) / (float)pow(min / 2, 2);

	if (A + B - 1 <= 0)
		return true;
	return false;
}

bool TEXT::checkPoint(int left, int top)
{
	if (left >= this->left - 10 && left <= this->left + wcsnlen_s(str, MAX_STRING_LENGHT) * 10 && top >= this->top - 10 && top <= this->top + 30)
		return true;
	return false;
}

void LINE::writeToFile(ofstream& fout)
{
	fout.write(reinterpret_cast<const char *>(&type), sizeof(unsigned int));
	fout.write(reinterpret_cast<const char *>(&left), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&top), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&right), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&bottom), sizeof(int));
	fout.write((char*)&color, sizeof(COLORREF));
}

void RECTANGLE::writeToFile(ofstream& fout)
{
	fout.write(reinterpret_cast<const char *>(&type), sizeof(unsigned int));
	fout.write(reinterpret_cast<const char *>(&left), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&top), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&right), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&bottom), sizeof(int));
	fout.write((char*)&color, sizeof(COLORREF));
}

void ELLIPSE::writeToFile(ofstream& fout)
{
	fout.write(reinterpret_cast<const char *>(&type), sizeof(unsigned int));
	fout.write(reinterpret_cast<const char *>(&left), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&top), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&right), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&bottom), sizeof(int));
	fout.write((char*)&color, sizeof(COLORREF));
}

void TEXT::writeToFile(ofstream& fout)
{
	fout.write(reinterpret_cast<const char *>(&type), sizeof(unsigned int));
	fout.write(reinterpret_cast<const char *>(&left), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&top), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&right), sizeof(int));
	fout.write(reinterpret_cast<const char *>(&bottom), sizeof(int));
	fout.write((char*)&color, sizeof(COLORREF));
	fout.write((char*)&font, sizeof(LOGFONT));
	fout.write(reinterpret_cast<const char *>(&sizeOfText), sizeof(int));
	fout.write((char*)str, MAX_STRING_LENGHT);
}