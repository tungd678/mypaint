#pragma once

#include "resource.h"
#include <iostream>
#include <string>
#include <commctrl.h>	
#include <Windows.h>
#include <commdlg.h>
#pragma comment(lib, "ComCtl32.lib")

HWND hwndMDIClient;
HWND hWndFrame;
HWND hWnd;
HWND hWndToolBar;
HWND hWndPrevEditBox;
HWND hWndPrevChild;
UINT currentCheck;
bool prevObject = false;
int childWndNum = 0;
UINT curState = ID_DRAW_LINE;
bool defaultCheckDrawLine = true;
bool textFinish = true;
bool selectObject = false;
bool resizeSelectObject = false;
unsigned int typeCopy = 0;
int left1, top1, right1, bottom1, type1, color1;