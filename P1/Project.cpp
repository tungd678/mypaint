// P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project.h"
#include "Objects.h"

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szDrawTitle[MAX_LOADSTRING];
WCHAR szDrawWindowClass[MAX_LOADSTRING];
WCHAR szFormat[20] = L"Object";

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    FrameWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    ChildWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK MDICloseProc(HWND hWnd, LPARAM lParam);
void inItFrameWindow(HWND hWnd);
void createMenuBar(HWND hWnd);
void createChildWnd(HWND hWnd);
void inItChildWindow(HWND hWnd);
void onActivate(HWND hWnd);
void onChildPaint(HWND hWnd);
void onLeftButtonUp(HWND hWnd, LPARAM lParam, int x1, int y1, int x2, int y2);
void onMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int& x2, int& y2);
void onTextInput(HWND hWnd, int x1, int y1, LPARAM lParam, HWND& hWndPrevEditBox);
void changeItemCheck(int wmID);
void createChooseColorBox(HWND hWnd);
void createChooseFontBox(HWND hWnd);
void createToolBar(HWND hWnd);
void addToToolBar();
void moveObject(HDC hdc, OBJECT* X, LPARAM lParam, int x1, int y1, int& x2, int& y2);
void resizeObject(HDC hdc, OBJECT* X, LPARAM lParam, int x1, int y1, int& x2, int& y2);
void createOpenFileBox(HWND hWnd);
void createSaveFileBox(HWND hWnd);
bool fileExists(string filename);
void outputChildWndDataToFile(ofstream& fout, CHILD_WND_DATA* data);
void onDeleteObject(HWND childwind);
void onCopy(HWND hWnd, HWND childwind);
void onPaste(HWND hWnd, HWND childwind);
void onCut(HWND hWnd, HWND childwind);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_CHILDWND, szDrawWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, (HWND) NULL, 0, 0))
    {
        if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hWndFrame, hAccelTable, &msg))
        {
			TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

	//Frame Window
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = FrameWndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if(!RegisterClassExW(&wcex))
		return false;

	//Child Window
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = ChildWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szDrawWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if (!RegisterClassExW(&wcex))
		return false;

	return 1;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

LRESULT CALLBACK FrameWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND childwind;
	switch (message)
	{
	case WM_CREATE:
		inItFrameWindow(hWnd);
		createMenuBar(hWnd);
		createToolBar(hWnd);
		addToToolBar();
		return 0;
	case WM_SIZE:
	{
		UINT w, h;
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		MoveWindow(hwndMDIClient, 0, 28, w, h, TRUE);
	}
	return 0;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		if (defaultCheckDrawLine)
			defaultCheckDrawLine = false;
		else {
			if (wmId != ID_DRAW_COLOR && wmId != ID_DRAW_FONT)
				curState = wmId;
		}
			
		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_NEW:
			changeItemCheck(ID_DRAW_LINE);
			inItChildWindow(hWnd);
			break;
		case ID_FILE_OPEN:
			createOpenFileBox(hWnd);
			break;
		case ID_FILE_SAVE:
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			createSaveFileBox(hWnd);
			break;
		case ID_DRAW_COLOR:
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			createChooseColorBox(hWnd);
			break;
		case ID_DRAW_FONT:
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			createChooseFontBox(hWnd);
			break;
		case ID_DRAW_LINE:
			changeItemCheck(wmId);
			break;
		case ID_DRAW_RECTANGLE:
			changeItemCheck(wmId);
			break;
		case ID_DRAW_ELLIPSE:
			changeItemCheck(wmId);
			break;
		case ID_DRAW_TEXT:
			changeItemCheck(wmId);
			break;
		case ID_DRAW_SELECTOBJECT:
			changeItemCheck(wmId);
			break;
		case ID_WINDOW_CASCADE:
			SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
			break;
		case ID_WINDOW_TIDE:
			SendMessage(hwndMDIClient, WM_MDITILE, 0, 0);
			break;
		case ID_WINDOW_CLOSEALL:
			EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
			break;
		case ID_EDIT_DELETE:
			changeItemCheck(ID_DRAW_LINE);
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			onDeleteObject(childwind);
			break;
		case ID_EDIT_COPY:
			changeItemCheck(ID_DRAW_LINE);
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			onCopy(hWndFrame, childwind);
			break;
		case ID_EDIT_CUT:
          		changeItemCheck(ID_DRAW_LINE);
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			onCut(hWnd, childwind);
			break;
		case ID_EDIT_PASTE:
			childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
			onPaste(hWnd, childwind);
			break;
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int x1, y1, x2, y2;
	switch (message)
	{
	case WM_CREATE:
		createChildWnd(hWnd);
		return 1;
	case WM_MDIACTIVATE:
		onActivate(hWnd);
		break;
	case WM_SIZE:
		return 1;
	case WM_PAINT:
		onChildPaint(hWnd);
		return 1;
	case WM_DESTROY:
		return 1;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		childWndNum--;
		break;
	case WM_LBUTTONDOWN:
	{
		selectObject = false;
		if (curState == ID_DRAW_TEXT && textFinish)
			textFinish = false;
		else if (curState == ID_DRAW_TEXT && !textFinish) {
			textFinish = true;
			CHILD_WND_DATA * data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			TEXT* new_text = new TEXT;
			new_text->left = x1;
			new_text->top = y1;
			new_text->font = data->font;
			new_text->color = data->color;
			new_text->type = 3;
			new_text->sizeOfText = GetWindowTextLengthW(hWndPrevEditBox) + 1;
			GetWindowTextW(hWndPrevEditBox, new_text->str, new_text->sizeOfText);
			new_text->str[new_text->sizeOfText] = '\0';
			data->Objects.push_back(new_text);
			return 0;
		}
		else if (curState == ID_DRAW_SELECTOBJECT && !selectObject)
		{
			CHILD_WND_DATA * data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			bool foundPoint = false;
			x1 = x2 = LOWORD(lParam);
			y1 = y2 = HIWORD(lParam);
			for (size_t i = data->Objects.size() - 1; i > 0; i--) {
				if (data->Objects[i]->checkPoint(x1, y1)) {
					foundPoint = true;
					OBJECT* tmp = data->Objects[i];
					for (size_t j = i; j < data->Objects.size() - 1; j++)
						data->Objects[j] = data->Objects[j + 1];
					i = data->Objects.size() - 1;
					data->Objects[i] = tmp;
					selectObject = true;
					//SendMessage(hWnd, WM_PAINT, 0, 0);
					return 0;
				}
			}
			if (data->Objects[0]->checkPoint(x1, y1) && !foundPoint) {
				OBJECT* tmp = data->Objects[0];
				for (size_t j = 0; j < data->Objects.size() - 1; j++)
					data->Objects[j] = data->Objects[j + 1];
				data->Objects[data->Objects.size() - 1] = tmp;
				selectObject = true;
				//SendMessage(hWnd, WM_PAINT, 0, 0);
				return 0;
			}
			selectObject = false;
		}
		else if (curState != ID_DRAW_SELECTOBJECT && selectObject)
			selectObject = false;
		x1 = x2 = LOWORD(lParam);
		y1 = y2 = HIWORD(lParam);
		if (curState == ID_DRAW_TEXT)
			onTextInput(hWnd, x1, y1, lParam, hWndPrevEditBox);
	}
		break;
	case WM_MOUSEMOVE:
		onMouseMove(hWnd, wParam,lParam, x1, y1, x2, y2);
		break;
	case WM_LBUTTONUP: 
	{
		onLeftButtonUp(hWnd, lParam, x1, y1, x2, y2);
		SendMessage(hWnd, WM_PAINT, 0, 0);
	}
		break;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}

void inItFrameWindow(HWND hWnd)
{
	hWndFrame = hWnd;
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	ccs.idFirstChild = 50001;
	hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL, 0, 0, 0, 0, hWnd, (HMENU)NULL, hInst, (LPVOID)&ccs);
	ShowWindow(hwndMDIClient, SW_SHOW);
}

void createMenuBar(HWND hWnd)
{
	currentCheck = ID_DRAW_LINE;
	HMENU hMenu = GetMenu(hWndFrame);
	EnableMenuItem(hMenu, ID_DRAW_COLOR, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_FONT, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_LINE, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_TEXT, MF_DISABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_DISABLED | MF_BYCOMMAND);
	CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);
	DrawMenuBar(hWndFrame);
}

void changeItemCheck(int wmID)
{
	HMENU hMenu = GetMenu(hWndFrame);
	if (wmID == currentCheck)
		return;
	CheckMenuItem(hMenu, currentCheck, MF_UNCHECKED | MF_BYCOMMAND);
	CheckMenuItem(hMenu, wmID, MF_CHECKED | MF_BYCOMMAND);
	currentCheck = wmID;
	DrawMenuBar(hWndFrame);
}

void createChildWnd(HWND hWnd)
{
	CHILD_WND_DATA *data;
	data = (CHILD_WND_DATA *)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	data->color = RGB(0, 0, 0);
	data->hWnd = hWnd;
	ZeroMemory(&(data->font), sizeof(LOGFONT));
	data->font.lfHeight = 16;
	data->FilePath = "C";
	wcscpy_s(data->font.lfFaceName, LF_FACESIZE, L"Times New Roman");
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, LONG_PTR(data)) == 0)
		if (GetLastError() != 0)
			MessageBox(hWnd, L"There's error!!!", L"ERROR", MB_OK);
}

void inItChildWindow(HWND hWnd)
{
	childWndNum++;
	wsprintf(szDrawTitle, L"Noname-%d.drw", childWndNum);
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = szDrawWindowClass;
	mdiCreate.szTitle = szDrawTitle;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
}

LRESULT CALLBACK MDICloseProc(HWND hWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hWnd, 0L);
	return 1;
}

void onActivate(HWND hWnd)
{
	CHILD_WND_DATA * Data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (Data == NULL)
		return;
	HMENU hMenu = GetMenu(hWndFrame);
	EnableMenuItem(hMenu, ID_DRAW_COLOR, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_FONT, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_LINE, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_TEXT, MF_ENABLED | MF_BYCOMMAND);
	EnableMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_ENABLED | MF_BYCOMMAND);
	CheckMenuItem(hMenu, currentCheck, MF_CHECKED | MF_BYCOMMAND);
	DrawMenuBar(hWndFrame);
}

void onChildPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	hdc = GetDC(hWnd);
	RECT rec = RECT();
	GetClientRect(hWnd, &rec);
	FillRect(hdc, &rec, (HBRUSH)(COLOR_WINDOW + 1));

	CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (data == NULL)
		return;

	if (selectObject) {
		HPEN hPen = CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
		SelectObject(hdc, hPen);
		
		for (int i = 0; i < data->Objects.size() - 1; i++) {
			hPen = CreatePen(PS_SOLID, 1, data->Objects[i]->color);
			SelectObject(hdc, hPen);
			data->Objects[i]->draw(hdc);
		}
		data->Objects[data->Objects.size() - 1]->draw(hdc);
		DeleteObject(hPen);
	}
	else {
		for (int i = 0; i < data->Objects.size(); i++) {
			HPEN hPen = CreatePen(PS_SOLID, 1, data->Objects[i]->color);
			SelectObject(hdc, hPen);
			data->Objects[i]->draw(hdc);
			DeleteObject(hPen);
		}
	}
	ReleaseDC(hWnd, hdc);
	EndPaint(hWnd, &ps);
}

void onLeftButtonUp(HWND hWnd, LPARAM lParam, int x1, int y1, int x2, int y2)
{
	CHILD_WND_DATA*data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (data == NULL)
		return;
	
	switch (curState)
	{
	case ID_DRAW_LINE:
	{
		LINE* new_line = new LINE;
		new_line->left = x1;
		new_line->top = y1;
		new_line->right = x2;
		new_line->bottom = y2;
		new_line->color = data->color;
		new_line->type = 0;
		data->Objects.push_back(new_line);
	}
		break;
	case ID_DRAW_RECTANGLE:
	{
		RECTANGLE* rec = new RECTANGLE;
		rec->left = x1;
		rec->top = y1;
		rec->right = x2;
		rec->bottom = y2;
		rec->color = data->color;
		rec->type = 1;
		data->Objects.push_back(rec);
	}
		break;
	case ID_DRAW_ELLIPSE:
	{
		ELLIPSE* ell = new ELLIPSE;
		ell->left = x1;
		ell->top = y1;
		ell->right = x2;
		ell->bottom = y2;
		ell->color = data->color;
		ell->type = 2;
		data->Objects.push_back(ell);
	}
		break;
	case ID_DRAW_SELECTOBJECT:
	{
		if (selectObject) {
			data->Objects[data->Objects.size() - 1]->left = abs(x2 - abs(x1 - data->Objects[data->Objects.size() - 1]->left));
			data->Objects[data->Objects.size() - 1]->top = abs(y2 - abs(y1 - data->Objects[data->Objects.size() - 1]->top));
			data->Objects[data->Objects.size() - 1]->right = abs(x2 + abs(data->Objects[data->Objects.size() - 1]->right - x1));
			data->Objects[data->Objects.size() - 1]->bottom = abs(y2 + abs(data->Objects[data->Objects.size() - 1]->bottom - y1));
			SendMessage(hWnd, WM_PAINT, 0, 0);
		}
	}
		break;
	}
}

void onMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int x1, int y1, int& x2, int& y2)
{
	if (wParam & MK_LBUTTON)
	{
		switch (curState)
		{
		case ID_DRAW_LINE:
		{
			HDC hdc = GetDC(hWnd);
			SetROP2(hdc, R2_NOTXORPEN);
			if (prevObject) {
				SetROP2(hdc, R2_NOTXORPEN);
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);
			}
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			MoveToEx(hdc, x1, y1, NULL);
			LineTo(hdc, x2, y2);
			prevObject = true;
			ReleaseDC(hWnd, hdc);
		}
			break;
		case ID_DRAW_RECTANGLE:
		{
			HDC hdc = GetDC(hWnd);
			SetROP2(hdc, R2_NOTXORPEN);
			if (prevObject) {
				SetROP2(hdc, R2_NOTXORPEN);
				Rectangle(hdc, x1, y1, x2, y2);
			}
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			Rectangle(hdc, x1, y1, x2, y2);
			prevObject = true;
			ReleaseDC(hWnd, hdc);
		}
			break;
		case ID_DRAW_ELLIPSE:
		{
			HDC hdc = GetDC(hWnd);
			SetROP2(hdc, R2_NOTXORPEN);
			if (prevObject) {
				SetROP2(hdc, R2_NOTXORPEN);
				Ellipse(hdc, x1, y1, x2, y2);
			}
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			Ellipse(hdc, x1, y1, x2, y2);
			prevObject = true;
			ReleaseDC(hWnd, hdc);
		}
			break;
		case ID_DRAW_SELECTOBJECT:
		{
			CHILD_WND_DATA * data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
			if (selectObject) {
				size_t i = data->Objects.size() - 1;
				HDC hdc = GetDC(hWnd);
				resizeObject(hdc, data->Objects[i], lParam, x1, y1, x2, y2);
				if(!resizeSelectObject)
					moveObject(hdc, data->Objects[i], lParam, x1, y1, x2, y2);
				ReleaseDC(hWnd, hdc);
			}
		}
			break;
		}
	}
}

void onTextInput(HWND hWnd, int x1, int y1, LPARAM lParam, HWND& hWndPrevEditBox)
{
	CHILD_WND_DATA*data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (data == NULL)
		return;
	HFONT hFont = CreateFontIndirect(&data->font);

	HWND editBox = CreateWindowW(L"Edit", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP, x1, y1, 300, abs(data->font.lfHeight) + 8, hWnd, nullptr, hInst, nullptr);
	SendMessage(editBox, WM_SETFONT, (WPARAM)hFont, TRUE);
	SetFocus(editBox);
	hWndPrevEditBox = editBox;
	DeleteObject(hFont);
	ShowWindow(hWnd, SW_NORMAL);
}

void createChooseColorBox(HWND hWnd)
{
	CHOOSECOLOR cc;                 // common dialog box structure 
	static COLORREF acrCustClr[16]; // array of custom colors 
	HBRUSH hbrush;                  // brush handle
	static DWORD rgbCurrent;        // initial color selection

									// Initialize CHOOSECOLOR 
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc) == TRUE)
	{
		CHILD_WND_DATA*data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		if (data == NULL)
			return;

		data->color = cc.rgbResult;
	}
}

void createChooseFontBox(HWND hWnd)
{
	CHOOSEFONT cf;
	static LOGFONT lf;
	static DWORD rgbCurrent;

	ZeroMemory(&cf, sizeof(cf));
	cf.lStructSize = sizeof(cf);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.rgbColors = rgbCurrent;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;

	if (ChooseFont(&cf) == TRUE)
	{
		CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		if (data == NULL)
			return;
		data->color = cf.rgbColors;
		data->font = lf;
	}
}

void createToolBar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
	{ STD_FILENEW, ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILEOPEN, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	hWndToolBar = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	CheckMenuItem(GetMenu(hWnd), IDM_TOOLBAR_VIEWHIDE, MF_CHECKED | MF_BYCOMMAND);
}

void addToToolBar()
{
	TBBUTTON tbButtons[] =
	{
	{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
	{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 1, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 2, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 3, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 4, ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	TBADDBITMAP	tbBitmap = {hInst, IDB_BITMAP1};
	int idx = SendMessage(hWndToolBar, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP), (LPARAM)(LPTBADDBITMAP)&tbBitmap);

	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;

	SendMessage(hWndToolBar, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON), (LPARAM)(LPTBBUTTON)&tbButtons);
}

void moveObject(HDC hdc, OBJECT* X, LPARAM lParam, int x1, int y1, int& x2, int& y2)
{
	if (X->type == 0) {
		SetROP2(hdc, R2_NOTXORPEN);
		MoveToEx(hdc, x2 - (x1 - X->left), y2 - (y1 - X->top), NULL);
		LineTo(hdc, x2 + (X->right - x1), y2 + (X->bottom - y1));
		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		MoveToEx(hdc, x2 - (x1 - X->left), y2 - (y1 - X->top), NULL);
		LineTo(hdc, x2 + (X->right - x1), y2 + (X->bottom - y1));
	}
	else if (X->type == 1) {
		SetROP2(hdc, R2_NOTXORPEN);
		Rectangle(hdc, abs(x2 - abs(x1 - X->left)), abs(y2 - abs(y1 - X->top)), abs(x2 + abs(X->right - x1)), abs(y2 + abs(X->bottom - y1)));
		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		Rectangle(hdc, abs(x2 - abs(x1 - X->left)), abs(y2 - abs(y1 - X->top)), abs(x2 + abs(X->right - x1)), abs(y2 + abs(X->bottom - y1)));
	}
	else if (X->type == 2) {
		SetROP2(hdc, R2_NOTXORPEN);
		Ellipse(hdc, abs(x2 - abs(x1 - X->left)), abs(y2 - abs(y1 - X->top)), abs(x2 + abs(X->right - x1)), abs(y2 + abs(X->bottom - y1)));
		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		Ellipse(hdc, abs(x2 - abs(x1 - X->left)), abs(y2 - abs(y1 - X->top)), abs(x2 + abs(X->right - x1)), abs(y2 + abs(X->bottom - y1)));
	}
}

void resizeObject(HDC hdc, OBJECT* X, LPARAM lParam, int x1, int y1, int& x2, int& y2)
{
	if (X->type == 0) {

	}
}

void createOpenFileBox(HWND hWnd)
{
	OPENFILENAME ofn;       // common dialog box structure
	WCHAR szFile[260];       // buffer for file name
	WCHAR* szFileTitle = new WCHAR[256];
	HANDLE hf;              // file handle


	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = szFile;
	// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
	// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = _T("DRAW (*.drw)\0*.drw*\0");
	ofn.lpstrDefExt = _T("drw");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.nMaxFileTitle = 256;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// Display the Open dialog box. 

	if (GetOpenFileName(&ofn) == TRUE) {
		int sizeOfVectorObj = 0;
		ifstream fin;
		fin.open(ofn.lpstrFile, ios::binary);
		SendMessage(hWnd, WM_COMMAND, ID_FILE_NEW, 0);

		CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		if (data == NULL)
			return;

		SetWindowText(hWnd, ofn.lpstrFileTitle);
		fin.read((char*)&data->color, sizeof(COLORREF));
		fin.read((char*)&data->font, sizeof(LOGFONT));
		fin.read((char*)&sizeOfVectorObj, sizeof(int));
		for (int i = 0; i < sizeOfVectorObj; i++) {
			unsigned int typeFile = 0;
			fin.read((char*)&typeFile, sizeof(unsigned int));
			
			if (typeFile != 3) {
				if (typeFile == 0) {
					LINE* new_data = new LINE;
					new_data->type = typeFile;
					fin.read((char*)&new_data->left, sizeof(int));
					fin.read((char*)&new_data->top, sizeof(int));
					fin.read((char*)&new_data->right, sizeof(int));
					fin.read((char*)&new_data->bottom, sizeof(int));
					fin.read((char*)&new_data->color, sizeof(COLORREF));
					data->Objects.push_back(new_data);
				}
				else if (typeFile == 1) {
					RECTANGLE* new_data = new RECTANGLE;
					new_data->type = typeFile;
					fin.read((char*)&new_data->left, sizeof(int));
					fin.read((char*)&new_data->top, sizeof(int));
					fin.read((char*)&new_data->right, sizeof(int));
					fin.read((char*)&new_data->bottom, sizeof(int));
					fin.read((char*)&new_data->color, sizeof(COLORREF));
					data->Objects.push_back(new_data);
				}
				else {
					ELLIPSE* new_data = new ELLIPSE;
					new_data->type = typeFile;
					fin.read((char*)&new_data->left, sizeof(int));
					fin.read((char*)&new_data->top, sizeof(int));
					fin.read((char*)&new_data->right, sizeof(int));
					fin.read((char*)&new_data->bottom, sizeof(int));
					fin.read((char*)&new_data->color, sizeof(COLORREF));
					data->Objects.push_back(new_data);
				}
			}
			else {
				TEXT* new_data = new TEXT;
				new_data->type = typeFile;
				fin.read((char*)&new_data->left, sizeof(int));
				fin.read((char*)&new_data->top, sizeof(int));
				fin.read((char*)&new_data->right, sizeof(int));
				fin.read((char*)&new_data->bottom, sizeof(int));
				fin.read((char*)&new_data->color, sizeof(COLORREF));
				fin.read((char*)&new_data->font, sizeof(LOGFONT));
				fin.read((char*)&new_data->sizeOfText, sizeof(int));
				fin.read((char*)new_data->str, MAX_STRING_LENGHT);
				data->Objects.push_back(new_data);
			}
		}
		fin.close();
	}
	delete[]szFileTitle;
}

void createSaveFileBox(HWND hWnd)
{
	OPENFILENAMEW ofn;       // common dialog box structure
	WCHAR szFile[500];       // buffer for file name
	WCHAR* szFileTitle = new WCHAR[256];
	HANDLE hf;              // file handle

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0'; // Set lpstrFile[0] to '\0' so that GetOpenFileName does not use the contents of szFile to initialize itself.
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = _T("DRAW (*.drw)\0*.drw*\0");
	ofn.lpstrDefExt = _T("drw");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.nMaxFileTitle = 256;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (data == NULL)
		return;

	if (fileExists(data->FilePath) == true) {
		ofstream fout;
		fout.open(data->FilePath, ios::binary);
		outputChildWndDataToFile(fout, data);
		fout.close();
	}
	else {
		if (GetSaveFileName(&ofn) == TRUE) {
			data->FilePath[0] = szFile[0];
			for (int i = 1; i < 500; i++) {
				if (szFile[i] != '\0')
					data->FilePath += szFile[i];
				else
					break;
			}
			ofstream fout;
			fout.open(ofn.lpstrFile, ios::binary);
			SetWindowTextW(hWnd, ofn.lpstrFileTitle);
			outputChildWndDataToFile(fout, data);
			fout.close();
		}
	}
	delete[]szFileTitle;
}

bool fileExists(string filename)
{
	ifstream ifile(filename);
	return ifile.good();
}

void outputChildWndDataToFile(ofstream& fout, CHILD_WND_DATA* data)
{
	fout.write((char*)&data->color, sizeof(COLORREF));
	fout.write((char*)&data->font, sizeof(LOGFONT));
	int sizeOfVector = data->Objects.size();
	fout.write((char*)&sizeOfVector, sizeof(int));
	for (int i = 0; i < data->Objects.size(); i++)
		data->Objects[i]->writeToFile(fout);
}

void onDeleteObject(HWND childwind)
{
	CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(childwind, 0);
	if (data == NULL)
		return;
	if (selectObject)
		data->Objects.pop_back();
	SendMessage(childwind, WM_PAINT, 0, 0);
}

void onCopy(HWND hWnd, HWND childwind)
{
	int id = RegisterClipboardFormat(szFormat);

	CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(childwind, 0);
	if (data == NULL)
		return;

	if (selectObject) 
	{
		if (data->Objects[data->Objects.size() - 1]->type == 0) 
		{
			typeCopy = 0;
			HGLOBAL hgbmain = GlobalAlloc(GHND, sizeof(OBJECT));
			OBJECT* p = (OBJECT*)GlobalLock(hgbmain);
			*p = *data->Objects[data->Objects.size() - 1];

			GlobalUnlock(hgbmain);
			if (OpenClipboard(hWndFrame)) {
				EmptyClipboard();
				SetClipboardData(id, hgbmain);
				CloseClipboard();
			}
		}
		else if (data->Objects[data->Objects.size() - 1]->type == 1)
		{
			typeCopy = 1;
			HGLOBAL hgbmain = GlobalAlloc(GHND, sizeof(OBJECT));
			OBJECT* p = (OBJECT*)GlobalLock(hgbmain);
			*p = *data->Objects[data->Objects.size() - 1];

			GlobalUnlock(hgbmain);
			if (OpenClipboard(hWndFrame)) {
				EmptyClipboard();
				SetClipboardData(id, hgbmain);
				CloseClipboard();
			}
		}
		else if (data->Objects[data->Objects.size() - 1]->type == 2) {
			typeCopy = 2;
			HGLOBAL hgbmain = GlobalAlloc(GHND, sizeof(OBJECT));
			OBJECT* p = (OBJECT*)GlobalLock(hgbmain);
			*p = *data->Objects[data->Objects.size() - 1];

			GlobalUnlock(hgbmain);
			if (OpenClipboard(hWndFrame)) {
				EmptyClipboard();
				SetClipboardData(id, hgbmain);
				CloseClipboard();
			}
		}
	}
}

void onPaste(HWND hWnd, HWND childwind)
{
	CHILD_WND_DATA* data = (CHILD_WND_DATA*)GetWindowLongPtr(childwind, 0);
	if (data == NULL)
		return;

	if (OpenClipboard(hWndFrame)) {
		int trid = RegisterClipboardFormat(L"Object");
		HGLOBAL hglobal = GetClipboardData(trid);
		if (hglobal)
		{
			if (typeCopy == 0) {
				OBJECT* l = (OBJECT*)GlobalLock(hglobal);
				left1 = l->left;
				top1 = l->top;
				right1 = l->right;
				bottom1 = l->bottom;
				color1 = l->color;
				type1 = l->type;
				GlobalUnlock(hglobal);
				LINE* newLine = new LINE;
				newLine->left = left1;
				newLine->top = top1;
				newLine->right = right1;
				newLine->bottom = bottom1;
				newLine->color = color1;
				newLine->type = type1;
				data->Objects.push_back(newLine);
				SendMessage(hWnd, WM_PAINT, 0, 0);
			}
			else if (typeCopy == 1) {
				OBJECT* l = (OBJECT*)GlobalLock(hglobal);
				left1 = l->left;
				top1 = l->top;
				right1 = l->right;
				bottom1 = l->bottom;
				color1 = l->color;
				type1 = l->type;
				GlobalUnlock(hglobal);
				RECTANGLE* newRec = new RECTANGLE;
				newRec->left = left1;
				newRec->top = top1;
				newRec->right = right1;
				newRec->bottom = bottom1;
				newRec->color = color1;
				newRec->type = type1;
				data->Objects.push_back(newRec);
				SendMessage(hWnd, WM_PAINT, 0, 0);
			}
			else if (typeCopy == 2) {
				OBJECT* l = (OBJECT*)GlobalLock(hglobal);
				left1 = l->left;
				top1 = l->top;
				right1 = l->right;
				bottom1 = l->bottom;
				color1 = l->color;
				type1 = l->type;
				GlobalUnlock(hglobal);
				ELLIPSE* newEll = new ELLIPSE;
				newEll->left = left1;
				newEll->top = top1;
				newEll->right = right1;
				newEll->bottom = bottom1;
				newEll->color = color1;
				newEll->type = type1;
				data->Objects.push_back(newEll);
				SendMessage(hWnd, WM_PAINT, 0, 0);
			}
		}
	}
	else
		MessageBox(hWnd, L"Không có dữ liệu", L"Lỗi", MB_OK);
	CloseClipboard();
}

void onCut(HWND hWnd, HWND childwind)
{
	onCopy(hWnd, childwind);
	onDeleteObject(childwind);
}