#pragma once
#include "Windows.h"
#include <string>
#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#define MAX_STRING_LENGHT 255
using namespace std;

class OBJECT
{
public:
	int left, top, right, bottom;
	COLORREF color;
	unsigned int type; //0=line, 1=rectangle, 2=ellipse, 3=text
	virtual void draw(HDC hdc) = 0;
	virtual bool checkPoint(int left, int top) = 0;
	virtual void writeToFile(ofstream& fout) = 0;
};

class LINE : public OBJECT
{
public:
	void draw(HDC hdc);
	bool checkPoint(int left, int top);
	void writeToFile(ofstream& fout);
};

class RECTANGLE : public OBJECT
{
public:
	void draw(HDC hdc);
	bool checkPoint(int left, int top);
	void writeToFile(ofstream& fout);
};

class ELLIPSE : public OBJECT
{
public:
	void draw(HDC hdc);
	bool checkPoint(int left, int top);
	void writeToFile(ofstream& fout);
};

class TEXT : public OBJECT
{
public:
	LOGFONT font;
	int sizeOfText;
	WCHAR str[MAX_STRING_LENGHT];

	void draw(HDC hdc);
	bool checkPoint(int left, int top);
	void writeToFile(ofstream& fout);
};

struct CHILD_WND_DATA
{
	HWND hWnd;
	COLORREF color;
	LOGFONT font;
	string FilePath;
	vector<OBJECT*> Objects;
};